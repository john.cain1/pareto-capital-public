import pandas as pd
import numpy as np

# GLOBAL_first_quartile = 0
# GLOBAL_third_quartile = 0

def f(row, first_quartile, third_quartile):
    if row < first_quartile:
        val = -5
    elif row < third_quartile: #vs row<=first_quartile
        val = 3
    elif row > third_quartile:
        val = 5
    else:
        val = 0 #this is the equivalent of FALSE on the google sheet

    return val

def tema_factor(df):
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)
    #print(GLOBAL_first_quartile)

    df['tema_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['tema_factor']

def bb_upper_factor(df):
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['bb_upper_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['bb_upper_factor']

def bb_middle_factor(df):
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['bb_middle_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['bb_middle_factor']

def bb_lower_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['bb_lower_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['bb_lower_factor']

def kc_upper_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['kc_upper_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['kc_upper_factor']

def kc_middle_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['kc_middle_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['kc_middle_factor']

def kc_lower_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['kc_lower_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['kc_lower_factor']

def volume_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['volume_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['volume_factor']

def rsi_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    print("RSI", GLOBAL_first_quartile)

    df['rsi_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['rsi_factor']

def mfi_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['mfi_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['mfi_factor']

def macd_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['macd_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['macd_factor']

def signal_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['signal_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['signal_factor']

def cmf_factor():
    GLOBAL_first_quartile = df.quantile(0.25)
    GLOBAL_third_quartile = df.quantile(0.75)

    df['cmf_factor'] = df.apply(f, args=(GLOBAL_first_quartile, GLOBAL_third_quartile))
    return df['cmf_factor']
