import btalib
import pandas as pd
import numpy as np


def sma(df, period):
    sma = btalib.sma(df, period=period)
    return sma.df

def ema(df, period):
    #firstClose = df['close'].iloc(0)
    ema = btalib.sma(df, period=period)
    return ema.df

def tema(df, length):
    ema1 = ema(df['close'], length)
    ema2 = ema(ema1, length)
    ema3 = ema(ema2, length)

    tema = ((3 * ema1) - (3 * ema2)) + ema3
    return tema

def mfi(df, length):
    mfi = btalib.mfi(df['high'], df['low'], df['close'], df['volume'], period=length)
    return mfi.df

def macd(df, fast_length, slow_length):
    #The MACD is calculated by subtracting the 26-period exponential moving average (EMA) from the 12-period EMA
    # firstClose = df['close'].iloc(0)
    # macd = btalib.macd(df['close'], _seed=3)
    # return macd.df
    ema12 = ema(df['close'], fast_length)
    ema26 = ema(df['close'], slow_length)
    macd = ema12-ema26
    return macd

def signal(macd_df, length):
    signal = ema(macd_df, length)
    #A nine-day EMA of the MACD called the "signal line,"
    return signal

def cmf(df, length):
    #Money Flow Multiplier = ((Close value – Low value) – (High value – Close value)) / (High value – Low value)
    mfm = ((df['close'] - df['low']) - (df['high'] - df['close'])) / (df['high'] - df['low'])

    #Money Flow Volume = Money Flow Multiplier x Volume for the Period
    mfv = mfm * df['volume']

    #CMF = 21-day Average of the Daily Money Flow / 21-day Average of the Volume
    cmf = sma(mfv, length) / sma(df['volume'], length)
    return cmf

#20, 40, 80 -> go to tradingview for all params
def uo(df, l1, l2, l3):
    uo = btalib.ultimateoscillator(df['high'], df['low'], df['close'], period1 = l1, period2 = l2, period3 = l3)
    return uo.df

# def ao(df):
#     median_price = (df['high']+df['low'])/2
#     sma5 = btalib.sma(median_price, period=5)
#     sma34 = btalib.sma(median_price, period=34)
#     ao = sma5.df-sma34.df
#     return ao

def rsi(df, period):
    diff = df['close'].diff(1).dropna()

    #this preservers dimensions off diff values
    up_chg = 0 * diff
    down_chg = 0 * diff

    up_chg[diff > 0] = diff[ diff>0 ]

    # down change is equal to negative deifference, otherwise equal to zero
    down_chg[diff < 0] = diff[ diff < 0 ]

    up_chg_avg   = up_chg.ewm(com=period-1 , min_periods=period).mean()
    down_chg_avg = down_chg.ewm(com=period-1 , min_periods=period).mean()

    rs = abs(up_chg_avg/down_chg_avg)
    rsi = 100 - 100/(1+rs)
    return rsi

def bbands(df, length, std):
    mid, top, bot = btalib.bbands(df['close'], period=length, devs=std)
    data = df.copy()
    data['mid'] = mid._series.to_frame()
    data['top'] = top._series.to_frame()
    data['bot'] = bot._series.to_frame()

    return {
        "middle_band":data['mid'],
        "upper_band":data['top'],
        "lower_band":data['bot']
    }

def keltner(df, length, atr_length):
    firstClose = df['close'].iloc(0)

    data = df.copy()

    data['keltnermiddle'] = ema(df['close'], length)

    high = data['high']
    low = data['low']
    close = data['close']
    data['tr0'] = abs(high - low)
    data['tr1'] = abs(high - close.shift())
    data['tr2'] = abs(low - close.shift())
    tr = data[['tr0', 'tr1', 'tr2']].max(axis=1)

    data['atr'] = btalib.smma(tr, period=atr_length, _seed=firstClose).df

    keltnermiddle = data['keltnermiddle']
    keltnerupper = data['keltnermiddle'] + (2 * data['atr'])
    keltnerlower = data['keltnermiddle'] - (2 * data['atr'])

    return {
        "keltner_middle": keltnermiddle,
        "keltner_upper": keltnerupper,
        "keltner_lower": keltnerlower
    }
