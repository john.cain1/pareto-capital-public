import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import plotly
import random
import plotly.graph_objs as go
from collections import deque
import pandas as pd
import plotly.express as px

import datetime

import index

df = index.build_chart_inputs()
close = df['close']
volume = df['index']

X = deque(maxlen=200)
X.append(1)
close_Y = deque(maxlen=200)
close_Y.append(close[0])
volume_Y = deque(maxlen=200)
volume_Y.append(volume[0])

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.DARKLY])
app.layout = html.Div(children=[
    html.Div(
        [
            html.H1(children='\tPareto Capital'),
        ]
    ),
    html.Div(
        [
            html.H4(id="balance"),
            dcc.Interval(
                id='interval-component',
                interval=1*1000, # in milliseconds
                n_intervals=0
            )
        ]
    ),
    html.Div(
        [
            html.Br(),
            dcc.Graph(id='price', animate=True),
            dcc.Interval(
                id='graph-update',
                interval=1000,
                n_intervals=0
            ),
        ]
    ),
    html.Div(
        [
            dcc.Graph(id='index', animate=True),
            dcc.Interval(
                id='volume-update',
                interval=1000,
                n_intervals=0,
            ),
        ]
    )
])

@app.callback(Output('balance', 'children'), Input('interval-component', 'n_intervals'))
def update_balance(n):
    wihtoutAlgo = 0
    withAlgo = 0
    return "Balance: ${}".format(3)

@app.callback(Output('price', 'figure'), [Input('graph-update', 'n_intervals')])
def update_graph_1(n):
    X.append(X[-1] + 1)
    close_Y.append(close[n])


    data = plotly.graph_objs.Scatter(
        x=list(X),
        y=list(close_Y),
        name='Scatter',
        mode='lines+markers'
    )

    return {'data': [data], 'layout': go.Layout(xaxis=dict(range=[min(X), max(X)]),
                                                yaxis=dict(range=[min(close_Y), max(close_Y)]),
                                                title="BITCOIN CLOSE",
                                                template="plotly_dark")}


@app.callback(Output('index', 'figure'), [Input('volume-update', 'n_intervals')])
def update_graph_2(n):
    volume_Y.append(volume[n])

    data = plotly.graph_objs.Scatter(
        x=list(X),
        y=list(volume_Y),
        name='Scatter',
        mode='lines+markers'
    )

    return {'data': [data], 'layout': go.Layout(xaxis=dict(range=[min(X), max(X)]),
                                                yaxis=dict(range=[min(volume_Y), max(volume_Y)]),
                                                title="NORMALIZED INDEX",
                                                height=300,
                                                template="plotly_dark")}


if __name__ == '__main__':
    app.run_server(debug=True)
